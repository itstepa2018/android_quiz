package a2018.by.step.myquiz.activity

import a2018.by.step.myquiz.R
import a2018.by.step.myquiz.data.FakeQuestionGenerator
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        tv_result.text = getString(
            R.string.result_text,
            (FakeQuestionGenerator.questionList.filter { it.isCorrect }.count() /
                    FakeQuestionGenerator.questionList.size.toDouble() * 100).toInt()
        )
    }
}
