package a2018.by.step.myquiz.data

import a2018.by.step.myquiz.model.User
import android.content.Context

object SharedPreferencesHelper {
    val propertiesName = "properties"
    private const val propertyNameName = "name"
    private const val propertyEmailName = "email"
    private const val propertyQuestionNumberName = "questionNumberName"
    private const val propertyWasRunBefore = "wasRunBefore"

    fun getUser(context: Context): User? {
        context.getSharedPreferences(propertiesName, Context.MODE_PRIVATE).apply {
            val name = this.getString(propertyNameName, null)
            val email = this.getString(propertyEmailName, name)
            if (name != null || email != null) {
                return User(name!!, email!!)
            } else {
                return null
            }
        }
    }

    fun setUser(context: Context, name: String, email: String) {
        context.getSharedPreferences(propertiesName, Context.MODE_PRIVATE).edit().apply {
            this.putString(propertyNameName, name)
            this.putString(propertyEmailName, email)
        }.apply()
    }

    fun setQuestionNumber(context: Context, number: Int) {
        context.getSharedPreferences(propertiesName, Context.MODE_PRIVATE).edit().apply {
            this.putInt(propertyQuestionNumberName, number)
        }.apply()
    }

    fun getQuestionNumber(context: Context): Int {
        context.getSharedPreferences(propertiesName, Context.MODE_PRIVATE).apply {
            return this.getInt(propertyQuestionNumberName, 1)
        }
    }

    fun wasRunBefore(context: Context): Boolean {
        context.getSharedPreferences(propertiesName, Context.MODE_PRIVATE).apply {
            val b = this.getBoolean(propertyWasRunBefore, false)
            this.edit().putBoolean(propertyWasRunBefore, true).apply()
            return b
        }
    }

    fun clearData(context: Context) {
        context.getSharedPreferences(propertiesName, Context.MODE_PRIVATE).edit().clear().apply()
    }
}