package a2018.by.step.myquiz.activity

import a2018.by.step.myquiz.R
import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_intro.*

class IntroActivity : BaseMenuActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)
        btn_start.setOnClickListener {
            startActivity(Intent(this, QuestionActivity::class.java))
            finish()
        }
    }
}
