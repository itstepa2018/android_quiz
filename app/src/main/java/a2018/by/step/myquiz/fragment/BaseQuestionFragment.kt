package a2018.by.step.myquiz.fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import timber.log.Timber

abstract class BaseQuestionFragment : Fragment() {
    var questionCallback: QuestionCallback? = null

    init {
        Timber.d("BaseQuestionFragment init")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("OnCreateMethod")
    }
}

interface QuestionCallback {
    fun onQuestionAnswered(id: Int, isCorrect: Boolean)
}