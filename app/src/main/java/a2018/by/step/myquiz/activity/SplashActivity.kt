package a2018.by.step.myquiz.activity

import a2018.by.step.myquiz.R
import a2018.by.step.myquiz.data.QuestionDataBaseRepository
import a2018.by.step.myquiz.data.SharedPreferencesHelper
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        startActivity(
            Intent(
                this,
                if (SharedPreferencesHelper.getUser(applicationContext) == null) {
                    LoginActivity::class.java
                } else {
                    IntroActivity::class.java
                }
            )
        )
//        val l = QuestionDataBaseRepository.getQuestions()
        finish()
    }
}
