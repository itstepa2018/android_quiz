package a2018.by.step.myquiz.fragment

import a2018.by.step.myquiz.R
import a2018.by.step.myquiz.model.ChoiceQuestion
import a2018.by.step.myquiz.model.TextQuestion
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_single_selection.*
import kotlinx.android.synthetic.main.fragment_single_selection.view.*
import timber.log.Timber

private const val ARG_QUESTION = "question"

class SingleSelectionFragment : BaseQuestionFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Timber.d("OnCreateViewMethod")
        val view = inflater.inflate(
            R.layout.fragment_single_selection,
            container, false
        )
        val question = arguments?.getParcelable<ChoiceQuestion>(ARG_QUESTION)
        view.tv_text_question.text = question?.text.toString()
        view.radio_btn_1.text = question!!.answers[0]
        view.radio_btn_2.text = question.answers[1]
        view.radio_btn_3.text = question.answers[2]
        view.radio_btn_4.text = question.answers[3]
        view.btn_next.setOnClickListener {
            question.userAnswer = when (view.rg_answers.checkedRadioButtonId) {
                R.id.radio_btn_1 -> 0
                R.id.radio_btn_2 -> 1
                R.id.radio_btn_3 -> 2
                R.id.radio_btn_4 -> 3
                else -> -1
            }
            questionCallback?.onQuestionAnswered(question.id, question.checkAnswer())
        }
        return view
    }

    override fun onStart() {
        super.onStart()
        Timber.d("OnStartMethod")
    }

    companion object {
        @JvmStatic
        fun newInstance(question: ChoiceQuestion) =
            SingleSelectionFragment().apply {
                retainInstance = true
                Timber.d("newInstance")
                arguments = Bundle().apply {
                    putParcelable(ARG_QUESTION, question)
                }
            }
    }

}