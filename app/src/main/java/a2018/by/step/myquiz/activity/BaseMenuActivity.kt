package a2018.by.step.myquiz.activity

import a2018.by.step.myquiz.R
import a2018.by.step.myquiz.data.FakeQuestionGenerator
import a2018.by.step.myquiz.data.QuestionDataBaseRepository
import a2018.by.step.myquiz.data.SharedPreferencesHelper
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog

abstract class BaseMenuActivity : AppCompatActivity() {

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            R.id.menu_logout -> logout()
            R.id.menu_hint -> {
                val questionNumber  = SharedPreferencesHelper.getQuestionNumber(applicationContext)
                val hint = QuestionDataBaseRepository.getQuestionById(questionNumber)!!.hint
                AlertDialog.Builder(this).setMessage(hint).
                    setTitle(R.string.hint).setPositiveButton(R.string.ok) { dialog, which ->  }.show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun logout() {
        SharedPreferencesHelper.clearData(applicationContext)
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}