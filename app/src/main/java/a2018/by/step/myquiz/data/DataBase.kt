package a2018.by.step.myquiz.data

import a2018.by.step.myquiz.model.ChoiceQuestion
import a2018.by.step.myquiz.model.Question
import a2018.by.step.myquiz.model.TextQuestion
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

private const val DB_NAME = "QuestionsDataBase"
private const val BD_VERSION = 1

private const val TABLE_QUESTIONS_NAME = "Questions"
private const val QUESTION_ID = "id"
private const val QUESTION_TEXT = "text"
private const val HINT = "hint"
private const val USER_ANSWER = "userAnswer"
private const val RIGHT_ANSWER = "rightAnswer"
private const val IS_CORRECT = "isCorrect"
private const val ANSWERS = "answers"

private const val QUERY_CREATE_QUESTION_TABLE = "create table $TABLE_QUESTIONS_NAME (" +
        "$QUESTION_ID integer primary key autoincrement," +
        "$QUESTION_TEXT text not null," +
        "$HINT text," +
        "$USER_ANSWER text," +
        "$RIGHT_ANSWER text not null," +
        "$IS_CORRECT integer not null default 0," +
        "$ANSWERS default null);"


class QuestionDBHelper(context: Context) :
    SQLiteOpenHelper(context, DB_NAME, null, BD_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(QUERY_CREATE_QUESTION_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("drop table if exists $TABLE_QUESTIONS_NAME")
        onCreate(db)
    }
}


object QuestionDataBaseRepository {
    lateinit var dB: SQLiteDatabase

    fun init(context: Context) {
        val questionDBHelper = QuestionDBHelper(context)
        dB = questionDBHelper.writableDatabase
    }

    fun addQuestion(question: Question<*>) {
        val answers: String? = if (question is ChoiceQuestion) {
            question.answers.joinToString(separator = "||")
        } else null
        val isCorrect = if (question.isCorrect) 1 else 0
        dB.execSQL(
            "insert into $TABLE_QUESTIONS_NAME " +
                    "($QUESTION_TEXT,$HINT,$USER_ANSWER,$RIGHT_ANSWER,$IS_CORRECT,$ANSWERS)" +
                    "values('${question.text}','${question.hint}','${question.userAnswer}'," +
                    "'${question.rightAnswer}',$isCorrect,'$answers');"
        )
    }

    fun getQuestions(): List<Question<*>> {
        return getAllQuestions("select * from $TABLE_QUESTIONS_NAME")
    }

    private fun getAllQuestions(sql: String): List<Question<*>> {
        val questionList = mutableListOf<Question<*>>()
        val cursor = dB.rawQuery(sql, null)
        while (cursor.moveToNext()) {
            val question: Question<*>
            val text = cursor.getString(cursor.getColumnIndex(QUESTION_TEXT))
            val hint = cursor.getString(cursor.getColumnIndex(HINT))
            val userAnswer = cursor.getString(cursor.getColumnIndex(USER_ANSWER))
            val rightAnswer = cursor.getString(cursor.getColumnIndex(RIGHT_ANSWER))
            val isCorrect = (cursor.getInt(cursor.getColumnIndex(IS_CORRECT)))
            val id = cursor.getInt(cursor.getColumnIndex(QUESTION_ID))
            val answers = cursor.getString(cursor.getColumnIndex(ANSWERS))
            if (answers == null || answers == "null") {
                question = TextQuestion(text, rightAnswer, hint)
                question.userAnswer = userAnswer
                //TODO Extension fun (convert Integer to boolean)
                question.isCorrect = isCorrect == 1
                question.id = id
            } else {
                question = ChoiceQuestion(
                    text, rightAnswer.toInt(), hint, answers.split("||")
                )
                question.userAnswer = if (userAnswer == null || userAnswer == "null") {
                    0
                } else {
                    userAnswer.toInt()
                }
                question.isCorrect = isCorrect == 1
                question.id = id
            }
            questionList.add(question)
        }
        cursor.close()
        return questionList
    }

    fun getQuestionById(id: Int): Question<*>? {
        val sql = "select * from $TABLE_QUESTIONS_NAME " +
                "where $QUESTION_ID = $id"
        return getAllQuestions(sql)[0]
    }
}