package a2018.by.step.myquiz.data

import a2018.by.step.myquiz.model.ChoiceQuestion
import a2018.by.step.myquiz.model.Question
import a2018.by.step.myquiz.model.TextQuestion

object FakeQuestionGenerator {
    val questionList = mutableListOf<Question<*>>()

    init {
        questionList.add(
            ChoiceQuestion(
                "Earth equator length?",
                3,
                "x<50000",
                listOf("10000", "20000", "30000", "40000")
            )
        )
        questionList.add(
            TextQuestion(
                "Minsk foundation year?",
                "1067",
                "After"
            )
        )
        questionList.add(
            TextQuestion(
                "Russia capital city?",
                "Moscow",
                "not Minsk"
            )
        )
        questionList.add(
            ChoiceQuestion(
                "What country does The river Nile resides in?",
                1,
                "Eg___t",
                listOf("Belarus", "Egypt", "Bangladesh", "England")
            )
        )
        questionList.add(
            TextQuestion(
                "The best Mobile OS on the Earth?",
                "Android",
                "not iOs"
            )
        )
    }

}