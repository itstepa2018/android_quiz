package a2018.by.step.myquiz

import a2018.by.step.myquiz.data.FakeQuestionGenerator
import a2018.by.step.myquiz.data.QuestionDataBaseRepository
import a2018.by.step.myquiz.data.SharedPreferencesHelper
import android.app.Application
import timber.log.Timber


class QuizApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        QuestionDataBaseRepository.init(applicationContext)
        if (!SharedPreferencesHelper.wasRunBefore(applicationContext)) {
            FakeQuestionGenerator.questionList.forEach {
                QuestionDataBaseRepository.addQuestion(it)
            }
        }

    }
}