package a2018.by.step.myquiz.activity

import a2018.by.step.myquiz.R
import a2018.by.step.myquiz.data.QuestionDataBaseRepository
import a2018.by.step.myquiz.data.SharedPreferencesHelper
import a2018.by.step.myquiz.fragment.BaseQuestionFragment
import a2018.by.step.myquiz.fragment.QuestionCallback
import a2018.by.step.myquiz.fragment.SingleSelectionFragment
import a2018.by.step.myquiz.fragment.TextQuestionFragment
import a2018.by.step.myquiz.model.ChoiceQuestion
import a2018.by.step.myquiz.model.TextQuestion
import android.content.Intent
import android.os.Bundle
import timber.log.Timber

class QuestionActivity : BaseMenuActivity(), QuestionCallback {

    override fun onQuestionAnswered(id: Int, isCorrect: Boolean) {
        Timber.d("on question #$id answer is $isCorrect")
        if (QuestionDataBaseRepository.getQuestions().size - 1 > id) {
            SharedPreferencesHelper.setQuestionNumber(applicationContext, id + 1)
            QuestionDataBaseRepository.getQuestionById(id)!!.isCorrect = isCorrect
            replaceFragment(id + 1)
        } else {
            SharedPreferencesHelper.setQuestionNumber(applicationContext, 0)
            startActivity(Intent(this, ResultActivity::class.java))
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Timber.d("OnCreate")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question)
        replaceFragment(SharedPreferencesHelper.getQuestionNumber(applicationContext))
    }

    private fun replaceFragment(id: Int) {
        val question = QuestionDataBaseRepository.getQuestionById(id)
        val fragment: BaseQuestionFragment
        when (question) {
            is TextQuestion -> fragment = TextQuestionFragment.newInstance(question)
            is ChoiceQuestion -> fragment = SingleSelectionFragment.newInstance(question)
            else -> throw ClassNotFoundException("Question of unsupported type")
        }
        fragment.questionCallback = this
        Timber.d("Question fragment is started: $question")
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.container_fragment,
                fragment
            )
            .commit()
    }

    override fun onStart() {
        Timber.d("OnStart")
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        Timber.d("OnResume ${hashCode()}")

    }

    override fun onPause() {
        Timber.d("OnPause ${hashCode()}")
        super.onPause()
    }

    override fun onStop() {
        Timber.d("OnStop")
        super.onStop()
    }

    override fun onDestroy() {
        Timber.d("OnDestroy")
        super.onDestroy()
    }
}
